import subprocess
import os
import sys
import argparse
import shutil
from subprocess import call
import yaml
from pydub import AudioSegment


def read_yaml(file_path):
    with open(file_path, "r") as f:
        return yaml.safe_load(f)

config = read_yaml("config.yaml")

app = config['APP']

# = ["¤M¤","¤N¤","¤P¤","¤S¤","¤T¤"]
anon_marks_string = app['ANON_MARKS_STRING'] 
anon_marks = anon_marks_string.split(",")

# buzz
buzzword = app['BUZZ_WORD'] 


dir_suffix = "AnonAfaire"

parser = argparse.ArgumentParser()

parser.add_argument("-ds", "--dir_suffix", help="directory suffix which will be explore.")
parser.add_argument("-id", "--input_dir", help="path of the directory containing the transcriptions and their sounds to anonymize.")
parser.add_argument("-isx", "--input_sound_extension", help="extension of the input sound to anonymize.")


args = parser.parse_args()


input_sound_extension = None

if len(sys.argv) == 2:
    sys.exit("Error : Please define the two parameter values : a directory path '-id' and an input sound extension '-isx'" )
else:
    for cmd_args in sys.argv[1:]:
        if cmd_args == "-isx":
            input_sound_extension = args.input_sound_extension
        if cmd_args == "-id":
            input_dir_path = args.input_dir    
        if cmd_args == "-ds":
            dir_suffix = args.dir_suffix    

# Converts the given file (.trs, .eaf or .cha formats) to TextGrid (Praat) format
def convertFileToTextGrid(file_path):

    if file_path.endswith(".trs"):
        call(["bash", "bin/transcribertopraat.sh", file_path])
    elif file_path.endswith(".eaf"):
        call(["bash", "bin/elantopraat.sh", file_path])  
    elif file_path.endswith(".cha"):
        call(["bash", "bin/clantopraat.sh", file_path])  
    else:
        sys.exit("Wrong file extension. Accepted extensions are .trs or elan.")


# Converts the files (.trs, .eaf or .cha formats) of the given directory to TextGrid (Praat) format
def convertTransciptionFilesToPraat(input_dir_path, dir_suffix):
    for root, dirs, files in os.walk(input_dir_path):
        print("root: ", root)
        if dir_suffix in root or files_contain_suffix(files, "_anon"):
            for filename in files :
                if filename.endswith(".trs") or filename.endswith(".eaf") or filename.endswith(".cha"):
                    file_path = root + os.sep + filename
                    convertFileToTextGrid(file_path)       


# Checks if the text contains an anonymisation mark.
def contains_anon_mark(text, anon_marks):
    
    for anon_mark in anon_marks:       
        if anon_mark in text: 
            return True
    return False    


# Replaces anonymisation marks by a buzz word in the given tg_file.
def put_buzzword_in_tg_file(tg_file, filedest, anon_marks, buzzword):
    fin = open(tg_file, "rt")
    fdest = open(filedest, "wt")

    for line in fin:
        if ("text =" in line or "mark =" in line) and contains_anon_mark(line, anon_marks):
            # line[:19] is equal to : "            text = " i.e 3 tabulation (12 spaces) before "text = "
            anon_line = line[:19] + '"' +   buzzword + '"' + "\n"
            #print('anon line: ', anon_line)
            fdest.write(anon_line)
        else:
            fdest.write(line)

    fin.close()                
    fdest.close()   

# Converts the given mp3 file to wav.
def convert_mp3_to_wav(mp3_file):
    # files 
    wav_file = mp3_file.replace(".mp3",".wav")

    # convert mp3 file to wav                                                            
    audSeg = AudioSegment.from_mp3(mp3_file)
    audSeg.export(wav_file, format="wav")


def files_contain_suffix(files, suffix):
    for file in files:
        if file.endswith(suffix + ".wav") :
            print(file)
            return True
    return False

# Anonymizes sound files of the given directory and its subdirectory which transcription contains one of the given anonymisation mark(anon_marks). 
def anonymize_dir_files(input_dir_path, dir_suffix, anon_marks, buzzword, input_sound_extension):
    
    for root, dirs, files in os.walk(input_dir_path):
        print('root:', root)   
                
            # replaces anonymisation marks by the given buzzword
           
        for filename in files :
            
            # Put buzz words in TextGrid (praat) Files 
            if filename.endswith(".TextGrid") :
                print('filename:', filename) 
                file_path_tmp = root + os.sep + filename  + "tmp"
                file_path = root + os.sep + filename
                shutil.copyfile(file_path, file_path_tmp)        
                put_buzzword_in_tg_file(file_path_tmp, file_path, anon_marks, buzzword)                
                os.remove(file_path_tmp) 

        # insert a buzz sound every time a buzzword is met in the transcription of the transcribed audio.
        call_sound_anonymizer(root, input_sound_extension, buzzword)

# Calls the Praat anonymisation script on the files of the given directory 
def call_sound_anonymizer(input_dir_path, input_sound_extension, buzzword):
    print("input_dir_path: ", input_dir_path)
    subprocess.call(['praat', '--run', 'anonymise_transcribed_sound.praat', input_dir_path, input_sound_extension, buzzword])


# input_sound_extension value by default 
if not input_sound_extension:
    input_sound_extension = ".wav"




if input_dir_path:
    # convert transription files of format 'trs' or 'eaf' to TextGrid (Praat file) format. 
    convertTransciptionFilesToPraat(input_dir_path, dir_suffix)
    anonymize_dir_files(input_dir_path, dir_suffix, anon_marks, buzzword,  input_sound_extension)
   

# python3 anonymise_transcribed_sound.py -isx ".wav" -id "transcription_dir_AnonAfaire"


# python3 anonymise_transcribed_sound.py -id "/home/driss/PostLab/Projets/ProjetDOC-CORLI/Corpus/"
# python3 anonymise_transcribed_sound.py -id "/home/driss/PostLab/Projets/ProjetDOC-CORLI/Corpus/CorpusAtraiter/DOC_FR_2020_OPINION_NN_1_AnonSonAfaire"
# python3 anonymise_transcribed_sound.py -isx ".mp3" -id "transcription_dir_AnonAfaire"
# python3 anonymise_transcribed_sound.py -isx ".mp3" -id "AnonOpinionES"
# python3 anonymise_transcribed_sound.py -isx ".wav" -id "/home/driss/PostLab/Projets/ProjetDOC-CORLI/Corpus/CorpusOpinion/CorpusOpinion_ES-OK/DOC_ES_2020_OPINION_10-AnonAfaire"