from distutils.log import error
import os
import sys
import argparse
import shutil
from subprocess import call

parser = argparse.ArgumentParser()

parser.add_argument("-d", "--dir", help="dir_path")
parser.add_argument("-f", "--inputfile", help="input transcription file with extension .trs or eaf")


args = parser.parse_args()

input_dir_path = None
file_path = None
trs_file_path = None
elan_file_path = None


if len(sys.argv) == 1:
    sys.exit("Error : Give a directory path -d")
else:
    for cmd_args in sys.argv[1:]:
        if cmd_args == "-d":
            input_dir_path = args.dir
        if cmd_args == "-f":
            file_path = args.inputfile

def convertFileToTextGrid(file_path):

    if file_path.endswith(".trs"):
        call(["bash", "bin/transcribertopraat.sh", file_path])
    elif file_path.endswith(".eaf"):
        call(["bash", "bin/elantopraat.sh", file_path])  
    else:
        sys.exit("Wrong file extension. Accpected extensions are .trs or elan.")



def transcriberFileToTextGrid(trs_file_path):

    if trs_file_path.endswith(".trs"):
        call(["bash", "bin/transcribertopraat.sh", trs_file_path])


def elanFileToTextGrid(elan_file_path):

    if elan_file_path.endswith(".eaf"):
        call(["bash", "bin/elantopraat.sh", elan_file_path])        

def transcriberDirectoryToTextGrid(dir_path):

    for subdir, dirs, files in os.walk(dir_path):
        #if "AnonSonAfaire" in subdir or "AnonSonAfaire" in dirs:
        for filename in files :
            if filename.endswith("Audio.trs"):
                filepath = subdir + os.sep + filename
                call(["bash", "bin/transcribertopraat.sh", filepath])

if file_path:
    convertFileToTextGrid(file_path)

if input_dir_path:
    transcriberDirectoryToTextGrid(input_dir_path)

# python3 convert_to_praat.py -d Anon
# python3 convert_to_praat.py -d AnonRencontreES
# python3 convert_to_praat.py -d AnonOpinionES
# python3 convert_to_praat.py -f DOC_FR_2020_CONSEIL_4_Transcription.trs
# python3 convert_to_praat.py -f débat_FR_1.eaf