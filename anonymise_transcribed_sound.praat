#praat script: anonymise_transcribed_sound.praat

#version: [2021-09-09]

#author: Driss Sadoun
#email: driss.sadoun@postlab.fr



#Adapted from version: [2013-03-06] of praat script: anonymise_long_sound.praat
#author: Daniel Hirst
#email: daniel.hirst@lpl-aix.fr
#dowload link : https://www.ortolang.fr/market/tools/sldr000526

#purpose: replace portions of a long sound which are labelled with a key word on the accompanying TextGrid
#		with a hum sound with the same prosodic characteristics as the original sound
#		Original long sound can be mono or stereo, anonymised sound will be same.

#requires: the folder containing the Long_Sounds to be anonymised may be specified or selected with the browser
#		each sound should be accompanied by a TextGrid with the same name



form anonymise_long_sound
	sentence folder 	
	word input_sound_extension
	word buzzword
endform


textGrid_extension$ = ".TextGrid"
anonymised_extension$ = "_anon" + input_sound_extension$
target_label$ = buzzword$
precision = 50
sound_output_format$ = "WAV"
# duration of section for analysis (in secs)
section = 30
timestep = 0.01
minimum_f0 = 60
maximum_f0 = 700
# use this to lower overall intensity if necessary
scale_intensity = 1

if input_sound_extension$ = "" 
	# The folder containing the Sounds and TextGrids to anonymise")
	input_sound_extension$ = ".wav"
endif	

# 1 stands for yes
automatic_max_and_min = 1 
# 1 stands for yes
verbose = 1 

if verbose
	clearinfo
endif



# Gets a list of files with the given sound extension.

sound_file_list = Create Strings as file list... Sounds 'folder$'/*'input_sound_extension$'

number_of_sound_files = Get number of strings



# Loops on each sound file of the given folder.
for current_sound_file to number_of_sound_files
	printline Script : 'current_sound_file$'
	select sound_file_list
	sound$ = Get string... current_sound_file

	# Checks that the current sound file is not already the result of an anomynisation.
	if not endsWith(sound$, anonymised_extension$)
		# "_Audio.wav" or "_Audio.mp3"
		sound_suffix$ = "_Audio" + input_sound_extension$
		# "_Transcription.TextGrid"
		textGrid_suffix$ = "_Transcription" + textGrid_extension$
		sound_name$ = sound$ - sound_suffix$
		textGrid$ = sound_name$ + textGrid_suffix$
		anonymised_sound$ = sound_name$ + "_Audio" + anonymised_extension$

		printline folder : 'folder$'
		printline textGrid : 'textGrid$'

		if not fileReadable(folder$+"/"+textGrid$)
			printline Can't find TextGrid file for 'name$'
		else
			call treat_sound
		endif
	endif
endfor

select sound_file_list
Remove



procedure treat_sound
	intermediate_file$ = ""
	if verbose
		printline Treating file 'sound$'
	endif
	sound_to_anonymise = Open long sound file... 'folder$'/'sound$'
	sound_duration = Get total duration
	sampling_frequency = Get sampling frequency
	myTextGrid = Read from file... 'folder$'/'textGrid$'

	totalDuration = Get total duration
	writeInfoLine: "Total duration: ", fixed$(totalDuration, 2), " seconds"
	# Gets the number of tiers (speakers) 
	numberOfTiers = Get number of tiers
	
	# loops on the different tiers
	for current_tier from 1 to numberOfTiers
		if numberOfTiers > 1 and current_tier < numberOfTiers 
			anonymised_sound$ = sound_name$ + "_Audio_tier_" + string$(current_tier) + anonymised_extension$
		else 	
			anonymised_sound$ = sound_name$ + "_Audio" + anonymised_extension$
		endif
		appendInfoLine: "anonymised_sound: ", anonymised_sound$
		# Checks that the current tier is not the first one to be processed.
		if current_tier > 1
			previous_anonymised_sound$ = sound_name$ + "_Audio_tier_" + string$(current_tier-1) + anonymised_extension$
			appendInfoLine: "previous_anonymised_sound: ", previous_anonymised_sound$
			intermediate_file$ = folder$ + "/" + previous_anonymised_sound$
			sound_to_anonymise = Open long sound file... 'folder$'/'previous_anonymised_sound$'
		endif
		
		select myTextGrid
		appendInfoLine: "Number of tiers is: ", numberOfTiers, "current tier is: ", current_tier
		# print out tier information one tier at a time
		tier_name$ = Get tier name: current_tier
		appendInfoLine: tab$, "Tier :", current_tier, """", tier_name$, """"
		if do( "Is interval tier...", current_tier )
			intervals = Get number of intervals: current_tier
			appendInfoLine: tab$, tab$, "there are ", intervals, " intervals."
		else
			points = Get number of points: 1
			appendInfoLine: tab$, tab$, "there are ", points, " points."
		endif

		
		select sound_to_anonymise
		part_end = 0
		#segment number
		iPart = 0

		repeat
			iPart = iPart+1
			part_start = part_end
			part_end = part_end + section
		
			if part_end > sound_duration
				part_end = sound_duration
			endif

			select sound_to_anonymise
			call treat_part current_tier numberOfTiers
		until part_end = sound_duration

		if current_tier > 1
			# Deletes intermediates anonymized files for the different tiers.
			# Only the final anonymized file (containing all processed tiers) is kept at the end.
			appendInfoLine: "Delete intermediate file: ", intermediate_file$
			deleteFile: intermediate_file$	
		endif

	endfor

	myNew_sound = Open long sound file... 'folder$'/'anonymised_sound$'
	pause - Click to continue
	select sound_to_anonymise
	plus myNew_sound
	plus myTextGrid
	Remove
endproc

procedure treat_part current_tier numberOfTiers
	nTargets = 0
	sound_to_anonymise_part = Extract part... part_start part_end no
	nChannels = Get number of channels
	intensity = Get intensity (dB)
	scaled_intensity = intensity * scale_intensity
	Scale intensity... scaled_intensity

	select myTextGrid
	myTextGrid_part = Extract part... part_start part_end no

	nIntervals = Get number of intervals... current_tier

	for iInterval to nIntervals
		select myTextGrid_part
		#appendInfoLine: "Get label of interval...", current_tier, " ", iInterval

		label$ = Get label of interval... current_tier iInterval

		label$ =  replace$(label$, " ","",0)
        # check if the current label is equal to the target label (to anonymise)	
		if label$ = target_label$			
			nTargets += 1
			call treat_word current_tier
		endif
	endfor

	select sound_to_anonymise_part


	if iPart = 1
		appendInfoLine: "iPart: ", iPart 
		Save as 'sound_output_format$' file... 'folder$'/'anonymised_sound$'
	else
		Append to existing sound file... 'folder$'/'anonymised_sound$'
	endif
	
	# Sets a string "anonymised sequences" or "anonymised sequence" if only one sequence was anonymised.
	anon$ = "anonymised sequence"
	if nTargets = 1
		anon$ = anon$ - "s"
	endif

	if verbose
		# Prints (after a tabulation) an informative line giving the number of anonymised sequences on each segment.
		printline 'tab$'• segment 'iPart' ['part_start:3'..'part_end:3'] 'nTargets' 'anon$'.
	endif

	plus myTextGrid_part
	Remove
endproc



procedure treat_word current_tier
	
	if automatic_max_and_min
		select sound_to_anonymise_part
		call calculate_min_max_f0
	else
		min_f0 = minimum_f0
		max_f0 = maximum_f0
	endif

####### initailise booleans for left or right parts
	left = 1
	right = 1
#######

	select myTextGrid_part
	
	word_start = Get start point... current_tier iInterval
	
	if word_start = 0
		left = 0
	endif
	
	word_end = Get end point... current_tier iInterval
	if word_end = section
		right = 0
	endif

	select sound_to_anonymise_part


	if left
		myLeft = Extract part... 0 word_start rectangular 1 no
	endif

	select sound_to_anonymise_part
	myWord = Extract part... word_start word_end rectangular 1 no
	myScale = Get intensity (dB)
	printline min_f0: 'min_f0'  .	
	printline max_f0: 'max_f0'  .	
	myPitch = To Pitch... timestep min_f0 max_f0
	#myHum_temp = Create Sound from formula... silence 1 0 0.5 sampling_frequency 0
	myHum_temp = To Sound (hum)
	myHum = Resample... sampling_frequency precision
	select myHum_temp
	Remove
	select myHum

	# Prints an informative line giving the number anonymised sequences on each segment.
	printline myWord: 'myWord'  .

	if nChannels = 2
		myHum_temp = Convert to stereo
		select myHum
		Remove
		myHum = myHum_temp
	endif

	select myWord
	myIntensity = To Intensity... min_f0 timestep no
	myIntensityTier = Down to IntensityTier
	plus myHum
	myNewHum = Multiply... yes
	if myScale != undefined
		Scale intensity... myScale
	endif

	select sound_to_anonymise_part
	if right
		myRight = Extract part... word_end section rectangular 1 no
	endif

	if left
		select myLeft
		plus myNewHum
	else
		select myNewHum
	endif

	if right
		plus myRight
	endif

	myNew_part = Concatenate
	select sound_to_anonymise_part
	if left
		plus myLeft
	endif
	plus myWord
	plus myPitch
	plus myHum
	plus myIntensity
	plus myIntensityTier
	plus myNewHum
	if right
		plus myRight
	endif
	Remove
	sound_to_anonymise_part = myNew_part
endproc

procedure calculate_min_max_f0
#  estimate of newMaxF0 as 2.5 * quantile 0.75
#  and newMinF0 as 0.5 * quantile 0.25
#  rounded to higher (resp. lower) 10
	To Pitch... 'timestep' 'minimum_f0' 'maximum_f0'
	.q75 = Get quantile... 0.0 0.0 0.75 Hertz
	.q25 = Get quantile... 0.0 0.0 0.25 Hertz
	max_f0 = 10*ceiling((2.5*.q75)/10)
	min_f0 = 10*floor((0.75*.q25)/10)
	Remove
endproc

#Version history

# [2013-03-07] remove leading or trailing spaces from labels
#			the folder containing the Sounds and TextGrids can be specified or selected with the browser
# [2013-03-06] corrected bug when label to anonymise crosses boundary of section
#			allow output in WAV, AIFF, AIFC, Next/SUN or NIST format
# [2011-03-25] changed extensions ".textGrid" to ".TextGrid"
#			changed max_f0 to 2.5 quantile 75 to allow for more expressive speech
#			resampled buzz to sampling rate of original sound
# [2010:05:24] allowed possibility of stereo long sounds
# [2008:05:25]	first version working.
