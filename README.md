# Titre : Audio transcription anonymizer

--

## Nomenclature de fichier à suivre

Les pairs de fichiers audio/transcription doivent suivre une certaine nomenclature. De sorte à simplifer l'identification  (dans un répertoire) des pairs audio/transcription pour les humains et les machines. Le script praat qui effectue l'édition d'un fichier audio pour remplacer les portions à anonymiser   par un bip, attend donc cette nomenclature, qui est la suivante :

>*Nom_enregistrement_Audio.wav*

>*Nom_enregistrement_Transcription.trs / Nom_enregistrement_Transcription.eaf  /  Nom_enregistrement_Transcription.TextGrid*

**Exemple**

>*T1-OUICHEF_20190913_Audio.wav*

>*T1-OUICHEF_20190913_Transcription.eaf*



## Structuration du code

Files : 
- README.md
- bin\
    - teicorpo.jar
    - transcribertopraat.sh

- anonymise_transcribed_sound.praat

- transcriber_to_praat.py

- insert _buzzword_in_trs.py

- anonymise_transcribed_sound.py


# Requirements

Install textgrid (https://github.com/kylebgorman/textgrid)

$ pip3 install textgrid
Defaulting to user installation because normal site-packages is not writeable
Collecting textgrid
  Downloading TextGrid-1.5-py3-none-any.whl (10.0 kB)
Installing collected packages: textgrid
Successfully installed textgrid-1.5


https://pypi.org/project/pydub/

pip install pydub


apt-get install ffmpeg

# anonymise_transcribed_sound.praat

Adapted from version: [2013-03-06] of praat script: anonymise_long_sound.praat
author: Daniel Hirst
email: daniel.hirst@lpl-aix.fr
dowload link : https://www.ortolang.fr/market/tools/sldr000526

purpose: replace portions of a long sound which are labelled with a key word on the accompanying TextGrid
		with a hum sound with the same prosodic characteristics as the original sound
		Original long sound can be mono or stereo, anonymised sound will be same.

requires: the folder containing the Long_Sounds to be anonymised may be specified or selected with the browser
		each sound should be accompanied by a TextGrid with the same name
