# python:3.6.9 for interface mode
FROM python:3.7.16

LABEL author="Driss Sadoun <driss.sadoun@postlab.fr>"
LABEL description="docker image anonymise transcription interface" 

RUN apt-get update --no-install-recommends && \
    apt-get install -y ffmpeg praat openjdk-11-jre && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

WORKDIR /app
COPY requirements.txt anonymise_transcribed_sound.py anonymise_transcribed_sound.praat /app/
RUN pip install --no-cache-dir -r requirements.txt

COPY bin /app/bin
COPY README.md LICENSE config.yaml /app/

ENTRYPOINT ["python3", "/app/anonymise_transcribed_sound.py"]