import os
import sys
import argparse
import shutil
from tabnanny import check
from subprocess import call




parser = argparse.ArgumentParser()

parser.add_argument("-id", "--inputdir", help="input directory path")
parser.add_argument("-od", "--outputdir", help="outpud directory path")
parser.add_argument("-f", "--TextGrid", help="input TextGrid file")

args = parser.parse_args()

input_dir_path = None
output_dir_path= None
tg_file= None

if len(sys.argv) == 1:
    sys.exit("Error : Give a directory output path -od")
else:
    for cmd_args in sys.argv[1:]:
        if cmd_args == "-id":
            input_dir_path = args.inputdir
        if cmd_args == "-od":
            output_dir_path = args.outputdir 
        if cmd_args == "-f":
            tg_file = args.TextGrid       

# Check if the text contains an anonymisation mark.
def contains_anon_mark(text, anon_marks):
    
    for anon_mark in anon_marks:       
        if anon_mark in text: 
            return True
    return False        


# Replaces anonymisation marks by a buzz word in the given tg_file.
def put_buzzword_in_tg_file(tg_file, filedest, anon_marks, buzzword):
    fin = open(tg_file, "rt")
    fdest = open(filedest, "wt")

    for line in fin:
        if ("text =" in line or "mark =" in line) and contains_anon_mark(line, anon_marks):
            # line[:19] is equal to : "            text = " i.e 3 tabulation (12 spaces) before "text = "
            anon_line = line[:19] + '"' +   buzzword + '"' + "\n"
            #print('anon line: ', anon_line)
            fdest.write(anon_line)
        else:
            fdest.write(line)

    fin.close()                
    fdest.close()   


            
    
     



# Copies content of directories containing files to anonymize
def copy_to_annon_directory_files(dir_path, dir_dest):
    try:
        os.mkdir(dir_dest)
    except OSError as error:
        print(error)

    for subdir, dirs, files in os.walk(dir_path):
        if "AnonSonAfaire" in subdir or "AnonAfaire" in subdir :
            for filename in files:
                if filename.endswith(".wav") or filename.endswith(".mp3") or filename.endswith(".trs"):
                    filepath = subdir + os.sep + filename
                    filedest = dir_dest + os.sep + filename
                    shutil.copy(filepath, filedest)


def put_buzzword_in_file_directory(dir_path, output_dir_path, buzzword):
    #anon_p = "€P€"
    anon_m = "¤M¤"
    anon_n = "¤N¤"
    anon_p = "¤P¤"
    anon_s = "¤S¤"
    anon_t = "¤T¤"

    if dir_path != "none":
        copy_to_annon_directory_files(dir_path, output_dir_path)

    for subdir, dirs, files in os.walk(output_dir_path):
        for filename in files:
            if filename.endswith("Transcription.trs"):
                buzz_filename = filename.replace("Transcription", "Audio")
                filepath = subdir + os.sep + filename
                filedest = subdir + os.sep + buzz_filename

                fin = open(filepath, "rt", encoding="ISO-8859-1")
                fdest = open(filedest, "wt", encoding="ISO-8859-1")

                for line in fin:
                    if anon_m in line:
                        fdest.write(buzzword)
                    elif anon_n in line:
                        fdest.write(buzzword)
                    elif anon_p in line:
                        fdest.write(buzzword)
                    elif anon_s in line:
                        fdest.write(buzzword)
                    elif anon_t in line:
                        fdest.write(buzzword)
                    else:
                        fdest.write(line)
                fin.close()
                fdest.close()



if tg_file:
    anon_marks = ["¤M¤","¤N¤","¤P¤","¤S¤","¤T¤"]
    put_buzzword_in_tg_file(tg_file, "anon.TextGrid", anon_marks, "buzz")


#copy_directory_files(input_dir_path, "Anon")
#put_buzzword_in_file_directory(input_dir_path, output_dir_path, "buzz")


# python3 insert_buzzword_in_praat.py -id DOC_FR_2020_CONSEIL_4_Transcription.TextGrid

# python3 insert_buzzword_in_praat.py -id ../../PostLab/Projets/ProjetDOC-CORLI/Corpus/ -od Anon
# python3 insert_buzzword_in_praat.py -id ../../PostLab/Projets/ProjetDOC-CORLI/Corpus/CorpusRencontre/CorpusRencontre_ES-OK/ -od AnonRencontreES
# python3 insert_buzzword_in_praat.py -id ../../PostLab/Projets/ProjetDOC-CORLI/Corpus/CorpusOpinion/CorpusOpinion_ES-OK/ -od AnonOpinionES
# python3 insert_buzzword_in_praat.py -od Anon
# find -iname "anon_*" | wc -l      : 51
# find . -iname '*anon_*' -delete
