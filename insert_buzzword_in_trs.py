import os
import sys
import argparse
import shutil
from subprocess import call

parser = argparse.ArgumentParser()

parser.add_argument("-id", "--inputdir", help="input directory path")
parser.add_argument("-od", "--outputdir", help="outpud directory path")

args = parser.parse_args()

input_dir_path = "none"


if len(sys.argv) == 1:
    sys.exit("Error : Give a directory output path -od")
else:
    for cmd_args in sys.argv[1:]:
        if cmd_args == "-id":
            input_dir_path = args.inputdir
        if cmd_args == "-od":
            output_dir_path = args.outputdir    

# Copies content of directories containing files to anonymize
def copy_to_annon_directory_files(dir_path, dir_dest):
    try:
        os.mkdir(dir_dest)
    except OSError as error:
        print(error)

    for subdir, dirs, files in os.walk(dir_path):
        if "AnonSonAfaire" in subdir or "AnonAfaire" in subdir :
            for filename in files:
                if filename.endswith(".wav") or filename.endswith(".mp3") or filename.endswith(".trs"):
                    filepath = subdir + os.sep + filename
                    filedest = dir_dest + os.sep + filename
                    shutil.copy(filepath, filedest)


def put_buzzword_in_file_directory(dir_path, output_dir_path, buzzword):
    #anon_p = "€P€"
    anon_m = "¤M¤"
    anon_n = "¤N¤"
    anon_p = "¤P¤"
    anon_s = "¤S¤"
    anon_t = "¤T¤"

    if dir_path != "none":
        copy_to_annon_directory_files(dir_path, output_dir_path)

    for subdir, dirs, files in os.walk(output_dir_path):
        for filename in files:
            if filename.endswith("Transcription.trs"):
                buzz_filename = filename.replace("Transcription", "Audio")
                filepath = subdir + os.sep + filename
                filedest = subdir + os.sep + buzz_filename

                fin = open(filepath, "rt", encoding="ISO-8859-1")
                fdest = open(filedest, "wt", encoding="ISO-8859-1")

                for line in fin:
                    if anon_m in line:
                        fdest.write(buzzword)
                    elif anon_n in line:
                        fdest.write(buzzword)
                    elif anon_p in line:
                        fdest.write(buzzword)
                    elif anon_s in line:
                        fdest.write(buzzword)
                    elif anon_t in line:
                        fdest.write(buzzword)
                    else:
                        fdest.write(line)
                fin.close()
                fdest.close()


#copy_directory_files(input_dir_path, "Anon")
put_buzzword_in_file_directory(input_dir_path, output_dir_path, "buzz")

# python3 insert_buzzword_in_trs.py -id ../../PostLab/Projets/ProjetDOC-CORLI/Corpus/ -od Anon
# python3 insert_buzzword_in_trs.py -id ../../PostLab/Projets/ProjetDOC-CORLI/Corpus/CorpusRencontre/CorpusRencontre_ES-OK/ -od AnonRencontreES
# python3 insert_buzzword_in_trs.py -id ../../PostLab/Projets/ProjetDOC-CORLI/Corpus/CorpusOpinion/CorpusOpinion_ES-OK/ -od AnonOpinionES
# python3 insert_buzzword_in_trs.py -od Anon
# find -iname "anon_*" | wc -l      : 51
# find . -iname '*anon_*' -delete
