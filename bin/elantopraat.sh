#!/bin/bash

java -cp bin/teicorpo.jar fr.ortolang.teicorpo.ElanToTei -i $1 -o tmp.tei_corpo.xml

echo $1

base=$(echo $1 | rev | cut -f 2- -d '.' | rev)

tg_file=$(echo "$base.TextGrid")

echo $tg_file

java -cp bin/teicorpo.jar fr.ortolang.teicorpo.TeiToPraat -i tmp.tei_corpo.xml -o $tg_file